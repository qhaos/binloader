#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


static const char usage[] =
"Usage: %s [OPTION]... file\n\n"
"\t-w              - set page to be writeable\n"
"\t-a address      - force the program to be loaded at a specfic address\n"
"\t                NOTE: this will purposefully fail if the address isn't available\n"
"\t-h              - prints this help message\n"
"\n\n";


int main(int argc, char* argv[]) {
  uintptr_t addr = (uintptr_t)NULL;
  char* addr_arg = NULL;
  int bin = 0;
  int ch;
  bool addr_flag = false, write_flag = false;

  int args = 1;

  while((ch = getopt(argc, argv, "wa:")) != -1) {
    args ++;
    switch(ch) {
      case 'w':
        write_flag = false;
      break;
      case 'a':
        addr_flag = true;
        addr_arg = optarg;
      break;
      case 'h':
        fprintf(stderr, usage, argv[0]);
        exit(EXIT_SUCCESS);
      break;
      case '?':
        fprintf(stderr, usage, argv[0]);
        exit(EXIT_FAILURE);
      break;
      default:
        abort();
      break;
    }
  }


  if(addr_arg) {
    char* end;
    addr = strtoull(addr_arg, &end, 0);
    if((!addr && (addr_arg[0] != '0')) || end-addr_arg-strlen(addr_arg) != 0) {
      fprintf(stderr, "-a expects a valid address, not %s\n", addr_arg);
      exit(EXIT_FAILURE);
    }
  }

  if(optind == argc) {
    fputs("binary file name not given but required\n", stderr);
    exit(EXIT_FAILURE);
  }

  bin = open(argv[optind], O_RDONLY);

  if(bin < 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  struct stat st;
  if(fstat(bin, &st)) {
    perror("fstat");
    exit(EXIT_FAILURE);
  }

  void* code = mmap((void*)addr, st.st_size, PROT_EXEC | PROT_READ | ((write_flag)? PROT_WRITE : 0),
    MAP_PRIVATE | ((addr_flag)? MAP_FIXED : 0), bin, 0
  );

  if(code == (void*) -1) {
    perror("mmap");
    if(addr_flag) {
      fputs("note: this is likely because the address you provided wasn't available\n", stderr);
    }
    exit(EXIT_FAILURE);
  }

  int ret = EXIT_FAILURE;

#if __x86_64
  __asm__("call *%%rax" : "=a"(ret) : "a"(code));
#else
  #error "this program does not support platforms other than AMD64/x86_64 ... for now"
#endif

  return ret;
}
