# Binloader
I wanted to be able to quickly run and load raw binary files, so, voila!

## Building
```sh
cc main.c -o binloader
```

Options:

-w = Make the memory your code is loaded into writeable (this may not work on some secured systems)  
-a = specify the memory address for your program to be loaded at (fails automatically if address cannot be secured)


## Programming Options
Your code does not *have* to be totally position independant, you can specify a starting address using the -a switch, BUT IF THIS ADDRESS IS NOT AVAILABLE, YOUR PROGRAM WILL NOT RUN.

No matter what, the address your program starts at will be in rax when your code is called initially.  
It does not take an abundance of imagination to figure out to use this to write something simple like a hello world program:

```x86asm
BITS 64
org 0x0000
mov rdi, 1
mov rsi, str
add rsi, rax
mov rax, 1
mov rdx, len
syscall

mov rax, 60
mov rdi, 0
syscall

str: db 'hello world', 0x0a
len equ $-str
```

However, it will be up to you to adjust your pointers accordingly!
