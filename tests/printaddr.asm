BITS 64
org 0x0000

lea rdx, [rax+hexstr]
mov rcx, 16
push 0x0a
loop:
sub rsp, 1
mov rdi, rax
and rdi, 0x0F
add rdi, rdx
shr rax, 4
mov bl, byte [rdi]
mov [rsp], bl
loop loop
sub rsp, 2
mov word[rsp], '0x'
mov rax, 1
mov rdi, 1
mov rdx, 19
mov rsi, rsp
syscall

mov rax, 60
mov rdi, 0
syscall
hexstr: db '0123456789ABCDEF',0
